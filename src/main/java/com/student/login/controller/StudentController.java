package com.student.login.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.student.login.model.StudentDTO;
import com.student.login.service.StudentService;

@RestController
@RequestMapping(value = "/student")
public class StudentController {
	@Autowired
	private StudentService service;
@GetMapping("/get")
public String home()
{
	return "home";
}
	@PostMapping("/add")
	public String addStudent(@RequestBody StudentDTO dto) {
		StudentDTO dto2 = service.registerStudent(dto);
		if (dto2 == null) {
			// return new ModelAndView("msg", "Please provide the Valid Information",
			// "register");
			return "give valid information";
		}
		// return new ModelAndView("msg", "Your Succesfully Registered", "Finally");
		return "added to database";
	}

	@GetMapping("/get")
	public List<StudentDTO> getAll() {

		// return new ModelAndView("get", "list", service.getAllService());
		return service.getAllService();
	}

	@GetMapping("/login/{email}/{password}")
	public String loginStudent(@PathVariable("email") String email, @PathVariable("password") String password) {
		boolean login = service.loginStudentService(email, password);
		if (login)
			return "Successfully Loged In Bro";
		return "Give Valid Credentials ";
	}

	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteStudent(@PathVariable("id") String id) {
		service.deleteStudentService(id);
		return new ModelAndView("delete");
	}

}