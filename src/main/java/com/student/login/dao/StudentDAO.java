package com.student.login.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.student.login.model.StudentDTO;
import com.student.login.repository.StudentRepository;

@Repository
public class StudentDAO {
	@Autowired
	private EntityManager entityManager;
	@Autowired
	private StudentRepository repository;

	public StudentDTO saveStudent(StudentDTO dto) {
		return repository.save(dto);
	}

	/*
	 * public StudentDTO loginStudent(String email) { if (email != null) { return
	 * (StudentDTO) entityManager.
	 * createQuery("from StudentDTO s where s.email=:email or s.phoneNo=:phoneNo")
	 * .setParameter("email", email).getSingleResult(); } else { return (StudentDTO)
	 * entityManager.createQuery("from StudentDTO s where s.phoneNo=:phoneNo")
	 * .setParameter("phoneNo", phoneNo).getSingleResult(); } }
	 */

	public StudentDTO getByEmailOrPassword(String email, String phoneNo) {

		return repository.findByEmail(email, phoneNo);
	}

	public StudentDTO getById(String id) {
		return repository.findById(id);
	}

	public void deleteStudent(String id) {
		StudentDTO dto = repository.findOne(id);
		repository.delete(dto);
	}

	public List<StudentDTO> getAll() {
		return repository.findAll();
	}

}
