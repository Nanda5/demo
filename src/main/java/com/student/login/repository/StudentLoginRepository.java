package com.student.login.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.student.login.model.StudentDTO;

public interface StudentLoginRepository extends JpaRepository<StudentDTO, String>{

}
