package com.student.login.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.student.login.model.StudentDTO;

@Repository
public interface StudentRepository extends JpaRepository<StudentDTO, String> {
	public StudentDTO findById(String id);

	@Query("from StudentDTO s where s.email=?1 or s.phoneNo=?2")
	public StudentDTO findByEmail(String email, String phoneNo);

}
