package com.student.login.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

@SuppressWarnings("serial")
@Entity
@Table(name = "student_table")
public class StudentDTO implements Serializable {
	@Id
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@GeneratedValue(generator = "uuid")
	@Column(name = "stu_id")
	private String id;
	@Column(name = "stu_name")
	@Size(min = 2, max = 18)
	@NotNull
	private String name;
	@Column(name = "stu_phoneno")
	@Size(max = 12)
	@NotNull
	private String phoneNo;
	@Column(name = "stu_email")
	@NotNull
	@Size(max = 20)
	private String email;
	@Column(name = "stu_subject")
	@NotNull
	@Size(max = 100)
	private String subject;
	@Column(name = "stu_address")
	@NotNull
	@Size(max = 250)
	private String address;
	@Column(name = "stu_gender")
	@NotNull
	@Size(max = 8)
	private String gender;
	@Column(name = "stu_cource")
	@NotNull
	@Size(max = 150)
	private String cource;
	@Column(name = "stu_password")
	@NotNull
	@Size(max = 50)
	private String password;
	@Column(name = "stu_confirmPassword")
	@Size(max = 50)
	private String confirmPassword;
	@Column(name = "stu_active")
	private boolean active;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCource() {
		return cource;
	}

	public void setCource(String cource) {
		this.cource = cource;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	@Override
	public String toString() {
		return "StudentDTO [id=" + id + ", name=" + name + ", phoneNo=" + phoneNo + ", email=" + email + ", subject="
				+ subject + ", address=" + address + ", gender=" + gender + ", cource=" + cource + ", password="
				+ password + ", confirmPassword=" + confirmPassword + ", active=" + active + "]";
	}

}
