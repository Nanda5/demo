package com.student.login.service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.student.login.dao.StudentDAO;
import com.student.login.model.StudentDTO;

@Service
public class StudentService {
	@Autowired
	private StudentDAO dao;

	public StudentDTO registerStudent(StudentDTO dto) {
		StudentDTO studentDTO = new StudentDTO();
		StudentDTO dto2 = dao.getByEmailOrPassword(dto.getEmail(), dto.getPhoneNo());
		String cource = getWithComma(dto.getCource());
		String subject = getWithComma(dto.getSubject());
		System.out.println(dto2);
		if (dto2 == null) {
			if (dto.getPassword().equals(dto.getConfirmPassword())) {
				BeanUtils.copyProperties(dto, studentDTO);
				studentDTO.setConfirmPassword(encryptPassword(dto.getConfirmPassword()));
				studentDTO.setPassword(encryptPassword(dto.getPassword()));
				studentDTO.setActive(true);
				studentDTO.setCource(cource);
				studentDTO.setSubject(subject);
				return dao.saveStudent(studentDTO);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public void deleteStudentService(String id) {
		dao.deleteStudent(id);
	}

	public List<StudentDTO> getAllService() {
		return dao.getAll();
	}

	public static String encryptPassword(String password) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(password.getBytes());
			BigInteger num = new BigInteger(1, messageDigest);
			String hashText = num.toString(10);
			while (hashText.length() < 32) {
				hashText = "0" + hashText;
			}
			return hashText;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	public boolean loginStudentService(String email, String password) {
		String pass = encryptPassword(password);
		StudentDTO dto = dao.getByEmailOrPassword(email, email);
		if (dto.getPassword().equals(pass))
			return true;
		return false;
	}

	public static String getWithComma(String comma) {
		char[] ch = comma.toCharArray();
		comma = "";
		for (int i = 0; i < ch.length; i++) {
			if (ch[i] == ' ') {
				comma += ",";
			}
			comma += ch[i];
		}
		return comma;
	}

	/*
	 * public static void main(String[] args) {
	 * System.out.println(getWithComma("nanda ok fine")); }
	 */

	/*
	 * public static void main(String[] args) { String st = "nanda123"; String ok =
	 * "Nanda234"; System.out.println(encryptPassword(st));
	 * System.out.println(encryptPassword(ok)); if
	 * (encryptPassword(st).equals(encryptPassword(ok))) {
	 * System.out.println(encryptPassword(st).equals(encryptPassword(ok))); } else {
	 * System.out.println("Nothin to print not same "); } }
	 */

}
